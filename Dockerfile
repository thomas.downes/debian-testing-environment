FROM debian:testing

LABEL name="Tom's working environment" \
      maintainer="Tom Downes <thomas.downes@ligo.org>"

RUN apt-get update && \
    apt-get install --assume-yes --no-install-recommends \
      nmap \
      tmux \
      vim && \
    rm -rf /var/lib/apt/lists/*
